import styled from "styled-components";

export const MainWrapper = styled.div`
  background-color: rgb(249, 249, 249);
`;
export const NavBar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #2c3e50;
`;
export const LogoWrapper = styled.div``;
export const Logo = styled.img`
  width: 100%;
  max-width: 5rem;
  margin-left: 2rem;
  border-radius: 50%;
  :hover {
    cursor: pointer;
    transform: scale(1.1);
  }
`;
export const AccWrapper = styled.div`
  font-size: 5rem;
  padding-right: 2.5rem;
  color: white;
  :hover {
    cursor: pointer;
    color: green;
    transform: scale(1.1);
  }
`;

// first sec
export const HomeWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2.3rem;
  flex-wrap: wrap;
  background-color: #2c3e50;
  color: white;
  /* background-color: #0e2b2e; */
`;
export const HomeTextWrapper = styled.div`
  flex: 1;
`;
export const HomeMainHeading = styled.div`
  font-size: 2rem;
  font-weight: 800;
  color: #777776;
`;
export const HomeFirstHeading = styled.div`
  font-size: 7.5vw;
  font-weight: 800;
`;
export const HomeSecHeading = styled.div`
  font-size: 7.5vw;
  font-weight: 800;
`;
export const HomeContent = styled.div`
  font-size: 1.8rem;
`;
export const AppLogoWrapper = styled.div`
  padding: 2rem 0;
  display: flex;
  flex-wrap: wrap;
  gap: 1.5rem;
`;
export const Android = styled.div``;
export const AndroidImg = styled.img`
  width: 100%;
  max-width: 12rem;
  min-height: 5rem;
`;
export const Iso = styled.div``;
export const IsoImg = styled.img`
  width: 100%;
  max-width: 13rem;
  min-height: 5rem;
`;

export const HomeImgWrapper = styled.div`
  :hover {
    transform: scale(1.1);
  }
`;
export const HomeImg = styled.img``;
// sec sec
export const AppWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;
export const AppTextWrapper = styled.div`
  width: 100%;
  max-width: 67rem;
  padding-top: 10rem;
`;
export const AppHeading = styled.div`
  font-family: tgn-soft-round-con, sans-serif;
  color: #0e2b2e;
  font-size: 48px;
  font-weight: 800;
  padding: 1rem;
`;
export const AppHeading2 = styled.div`
  font-family: "Source Sans Pro", sans-serif;
  font-size: 2rem;
  color: #485d5f;
  padding: 1rem;
`;
export const Appbenefits = styled.div`
  font-family: "Source Sans Pro", sans-serif;
  color: #0e2b2e;
  font-size: 2rem;
  padding: 1rem;
`;
export const AppImgWrapper = styled.div`
  padding-left: 3rem;
  :hover {
    transform: scale(1.1);
  }
`;
export const AppImg = styled.img`
  width: 100%;
  max-width: 40rem;
  height: auto;
`;

// third sec
export const ConnectAndLocateMainWrapper = styled.div`
  padding: 2vw;
`;
export const ConnectTextWrapper = styled.div`
  font-size: 3rem;
  font-weight: 500;
  padding: 0.5vw 0;
  color: #777776;
`;
export const ImgWrapper = styled.div``;
export const Img = styled.img`
  width: 100%;
`;
export const SvgAndTextWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 3rem;
  flex-wrap: wrap;
  background-color: #333;
  margin-top: -3px;
`;
export const SvgWrapper = styled.div`
  :hover {
    transform: scale(-0.9);
  }
`;
export const Svg = styled.img`
  width: 100%;
`;
export const TextWrapper = styled.div`
  color: white;
`;
export const Heading = styled.div`
  font-size: 3.5rem;
`;
export const Content = styled.div`
  font-size: 1.5rem;
  padding-bottom: 1vw;
`;

// aboutus
export const AboutusWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 4rem 5rem;
  gap: 7rem;
  background-color: #333;
`;
export const AboutImgWrapper = styled.div`
  width: 100%;
  max-width: 40rem;
  :hover {
    transform: scale(1.1);
  }
`;
export const AboutImg = styled.img`
  width: 100%;
  max-width: 40rem;
  height: auto;
  border: 1px solid #fff;
  border-radius: 2px;
  box-shadow: 10px 10px 5px #ccc;
`;
export const AboutTextWrapper = styled.div`
  width: 100%;
  max-width: 55rem;
  color: white;
  :hover {
    transform: scale(1.1);
  }
`;
export const VisionText = styled.div`
  font-size: 2rem;
  font-weight: 500;
  text-transform: uppercase;
  color: #777776;
`;
export const HeadingText = styled.div`
  font-size: 4rem;
  font-weight: 700;
  padding-bottom: 1rem;
  text-align: center;
`;
export const AboutContent = styled.div`
  font-size: 2.5rem;
  font-weight: 300;
  padding-left: 1rem;
`;
