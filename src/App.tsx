import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from "./pages/dashBoard";
import Landing from "./pages/landingPage";
import Login from "./pages/login";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
