// import logo from "assets/images/logo.png";
import {
  Button,
  Input,
  InputAndBtnWrapper,
  InputWrapper,
  LoginWrapper,
  Logo,
  LogoWrapper,
  MainWrapper,
  SignIn,
} from "styles/login";

import Evlogo from "assets/evlogo.jpeg";

const Login = () => {
  return (
    <MainWrapper>
      <LoginWrapper>
        <LogoWrapper>
          <Logo src={Evlogo} />
          <SignIn>Sign In</SignIn>
        </LogoWrapper>
        <InputAndBtnWrapper>
          <InputWrapper>
            <Input type="email" placeholder="Email Address" />
          </InputWrapper>
          <InputWrapper>
            <Input type="password" placeholder="Password" />
          </InputWrapper>
          <Button>Sign In</Button>
        </InputAndBtnWrapper>
      </LoginWrapper>
    </MainWrapper>
  );
};

export default Login;
