import Image from "assets/img1.webp";
import SvgImg from "assets/svg1.svg";
import ImgTwo from "assets/img2.png";
import AppStore from "assets/appstore.svg";
import PlayStore from "assets/playstore.svg";
import OurApp from "assets/ourapp.png";
import { TiTick } from "react-icons/ti";
import { MdAccountCircle } from "react-icons/md";
import Evlogo from "assets/evlogo.jpeg";
import Vision from "assets/future.png";

import {
  AboutContent,
  AboutImg,
  AboutImgWrapper,
  AboutTextWrapper,
  AboutusWrapper,
  AccWrapper,
  Android,
  AndroidImg,
  Appbenefits,
  AppHeading,
  AppHeading2,
  AppImg,
  AppImgWrapper,
  AppLogoWrapper,
  AppTextWrapper,
  AppWrapper,
  ConnectAndLocateMainWrapper,
  ConnectTextWrapper,
  Content,
  Heading,
  HeadingText,
  HomeContent,
  HomeFirstHeading,
  HomeImg,
  HomeImgWrapper,
  HomeMainHeading,
  HomeSecHeading,
  HomeTextWrapper,
  HomeWrapper,
  Img,
  ImgWrapper,
  Iso,
  IsoImg,
  Logo,
  LogoWrapper,
  MainWrapper,
  NavBar,
  Svg,
  SvgAndTextWrapper,
  SvgWrapper,
  TextWrapper,
  VisionText,
} from "styles/landingPage";
import { Link } from "react-router-dom";

const Landing = () => {
  return (
    <MainWrapper>
      <NavBar>
        <LogoWrapper>
          <Logo src={Evlogo} alt="Logo" />
        </LogoWrapper>
        <AccWrapper>
          <Link to="/login">
            <MdAccountCircle />
          </Link>
        </AccWrapper>
      </NavBar>
      {/* first sec */}
      <HomeWrapper>
        <HomeTextWrapper>
          <HomeMainHeading>Smart EV Charging App</HomeMainHeading>
          <HomeFirstHeading>PLUG INTO</HomeFirstHeading>
          <HomeSecHeading>BETTER.</HomeSecHeading>
          <HomeContent>
            80% of your charging takes place at home. Make it count.
          </HomeContent>
          <AppLogoWrapper>
            <Android>
              <AndroidImg src={AppStore} alt="AndroidImg" />
            </Android>
            <Iso>
              <IsoImg src={PlayStore} alt="IsoImg" />
            </Iso>
          </AppLogoWrapper>
        </HomeTextWrapper>
        <HomeImgWrapper>
          <HomeImg src={ImgTwo} alt="HomeImg" />
        </HomeImgWrapper>
      </HomeWrapper>

      {/* third sec */}
      <ConnectAndLocateMainWrapper>
        <ConnectTextWrapper>Locate Connect & Charge...</ConnectTextWrapper>
        <ImgWrapper>
          <Img src={Image} alt="img" />
        </ImgWrapper>
        <SvgAndTextWrapper>
          <SvgWrapper>
            <Svg src={SvgImg} alt="img" />
          </SvgWrapper>
          <TextWrapper>
            <Heading>One for all</Heading>
            <Content>
              Compatible with electric vehicles that use a 5A charger.
            </Content>
            <Heading>Find Charging Points on the app</Heading>
            <Content>
              Land at the nearest charger. Scan, verify and start charging using
              the Ev app. Simple.
            </Content>
          </TextWrapper>
        </SvgAndTextWrapper>
      </ConnectAndLocateMainWrapper>

      {/* section sec */}
      <AppWrapper>
        <AppTextWrapper>
          <AppHeading>Full control, in the palm of your hand</AppHeading>
          <AppHeading2>
            Automatically charge your vehicle with the cheapest and cleanest
            electricity available in your area.
          </AppHeading2>
          <Appbenefits>
            <TiTick />
            Save money on your energy bills
          </Appbenefits>
          <Appbenefits>
            <TiTick />
            Track your charging costs and kWh
          </Appbenefits>
          <Appbenefits>
            <TiTick />
            Claim cash rewards for Smart Charging
          </Appbenefits>
          <Appbenefits>
            <TiTick />
            Charge with your home solar generation
          </Appbenefits>
          <Appbenefits>
            <TiTick />
            Get award-winning customer support
          </Appbenefits>
        </AppTextWrapper>
        <AppImgWrapper>
          <AppImg src={OurApp} alt="#" />
        </AppImgWrapper>
      </AppWrapper>
      {/* abouus sec */}
      <AboutusWrapper>
        <AboutImgWrapper>
          <AboutImg src={Vision} alt="vision" />
        </AboutImgWrapper>
        <AboutTextWrapper>
          <VisionText>Our Vision</VisionText>
          <HeadingText>The Future Is Now.</HeadingText>
          <AboutContent>
            We envisage an India where a climate-conscious public utilise
            electric vehicles without compromising on convenience, or
            sustainability. Our desire to realise this dream is driven by
            everything we do, from our products, to partners.
          </AboutContent>
        </AboutTextWrapper>
      </AboutusWrapper>
    </MainWrapper>
  );
};

export default Landing;
